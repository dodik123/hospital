<?php

class Dashboard extends MX_Controller {

 public function __construct() {
  parent::__construct();
  if ($this->session->userdata('id') == '') {
   redirect(base_url());
  }
 }

 public function getModuleName() {
  return 'dashboard';
 }

 public function index() {
  $guru = $this->session->userdata('id');
  $data['view_file'] = 'dashboard_index_view';
  $data['module'] = $this->getModuleName();
  $data['data_pasien_hidup'] = $this->getDataGrafikPasienHidup();
  $data['data_pasien_meninggal'] = $this->getDataGrafikPasienMeninggal();
  $data['title'] = ucwords(str_replace('_', ' ', $this->getModuleName()));
  echo Modules::run('template', $data);
 }

 public function getDataGrafikPasienHidup($keyword = '') {
  $date = $keyword == '' ? date('Y') : $keyword;
  
  $data = Modules::run('database/get', array(
  'table' => 'pasien p',
  'join' => array(
  array('status_pasien sp', 'p.status_pasien = sp.id')
  ),
  'is_or_like' => true,
  'like' => array(
  array('p.tanggal_registrasi', $date)
  ),
  'where' => "sp.status != 'Meninggal'"
  ));


  $januari = 0;
  $februari = 0;
  $maret = 0;
  $april = 0;
  $mei = 0;
  $juni = 0;
  $juli = 0;
  $agustus = 0;
  $september = 0;
  $oktober = 0;
  $november = 0;
  $desember = 0;

  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    list($tgl, $bulan, $tahun) = explode('-', $value['tanggal_registrasi']);

    switch ($bulan) {
     case '01':
      $januari += 1;
      break;
     case '02':
      $februari += 1;
      break;
     case '03':
      $maret += 1;
      break;
     case '04':
      $april += 1;
      break;
     case '05':
      $mei += 1;
      break;
     case '06':
      $juni += 1;
      break;
     case '07':
      $juli += 1;
      break;
     case '08':
      $agustus += 1;
      break;
     case '09':
      $september += 1;
      break;
     case '10':
      $oktober += 1;
      break;
     case '11':
      $november += 1;
      break;
     case '12':
      $november += 1;
      break;
    }
   }
  }

//  return $result;
  return "" . $januari . "," . $februari . "," . $maret . ","
  . "" . $april . "," . $mei . "," . $juni . "," . $juli . "," . $agustus . ","
  . "" . $september . "," . "" . $oktober . "," . "" 
  . $november . "," . "" . $desember . "";
 }
 
 public function getDataGrafikPasienMeninggal($keyword = '') {
  $date = $keyword == '' ? date('Y') : $keyword;
  
  $data = Modules::run('database/get', array(
  'table' => 'pasien p',
  'join' => array(
  array('status_pasien sp', 'p.status_pasien = sp.id')
  ),
  'is_or_like' => true,
  'like' => array(
  array('p.tanggal_registrasi', $date)
  ),
  'where' => "sp.status = 'Meninggal'"
  ));


  $januari = 0;
  $februari = 0;
  $maret = 0;
  $april = 0;
  $mei = 0;
  $juni = 0;
  $juli = 0;
  $agustus = 0;
  $september = 0;
  $oktober = 0;
  $november = 0;
  $desember = 0;

  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    list($tgl, $bulan, $tahun) = explode('-', $value['tanggal_registrasi']);

    switch ($bulan) {
     case '01':
      $januari += 1;
      break;
     case '02':
      $februari += 1;
      break;
     case '03':
      $maret += 1;
      break;
     case '04':
      $april += 1;
      break;
     case '05':
      $mei += 1;
      break;
     case '06':
      $juni += 1;
      break;
     case '07':
      $juli += 1;
      break;
     case '08':
      $agustus += 1;
      break;
     case '09':
      $september += 1;
      break;
     case '10':
      $oktober += 1;
      break;
     case '11':
      $november += 1;
      break;
     case '12':
      $november += 1;
      break;
    }
   }
  }

//  return $result;
  return "" . $januari . "," . $februari . "," . $maret . ","
  . "" . $april . "," . $mei . "," . $juni . "," . $juli . "," . $agustus . ","
  . "" . $september . "," . "" . $oktober . "," . "" 
  . $november . "," . "" . $desember . "";
 }

}

<!--<nav> sdasdasdasd </nav>-->

<ul id="slide-out" class="sidenav">
 <li><div class="user-view">
   <div class="background" style="background-color: #08c">
<!--    <img src="<?php echo base_url() ?>files/images/office.jpg">-->

   </div>
   <br/>
   <a href="#user"><img class="circle" src="<?php echo base_url() ?>files/img/avatar5.png"></a>
   <a href="#name"><span class="white-text name">Username : <?php echo strtoupper($this->session->userdata('username')); ?></span></a>
   <a href="#hak_akses"><span class="white-text email">Hak Akses : <?php echo strtoupper($this->session->userdata('access')); ?></span></a>
  </div></li>
 <li>
  <a href="<?php echo base_url() . 'dashboard' ?>"><i class="mdi mdi-view-dashboard"></i>Dashboard</a>
 </li>
 <!-- <li><a href="#!">Second Link</a></li>-->
</li>

<?php if ($this->session->userdata('access') == 'Super Admin') {
 ?>
 <li><a class="subheader">Master</a></li> 
 <li>  
  <a class="waves-effect" href="<?php echo base_url() . 'dokter' ?>"><i class="mdi mdi-menu"></i>Dokter
   <span class="badge badge-success pull-right"></span>
  </a>
 </li>
 <li>  
  <a class="waves-effect" href="<?php echo base_url() . 'jumlah_foto' ?>"><i class="mdi mdi-menu"></i>Jumlah Foto
   <span class="badge badge-success pull-right"></span>
  </a>
 </li>
 <li>  
  <a class="waves-effect" href="<?php echo base_url() . 'user' ?>"><i class="mdi mdi-menu"></i>User
   <span class="badge badge-success pull-right"></span>
  </a>
 </li>
<?php } ?>

 <?php if ($this->session->userdata('access') == 'Admin' || $this->session->userdata('access') == 'Super Admin') {
 ?>
 <li><a class="subheader">Administrasi Pasien</a></li> 
 <li>  
  <a class="waves-effect" href="<?php echo base_url() . 'pasien' ?>"><i class="mdi mdi-menu"></i>Pasien
   <span class="badge badge-success pull-right"></span>
  </a>
 </li>
 </li>
<?php } ?>

<?php if ($this->session->userdata('access') == 'Dokter' || $this->session->userdata('access') == 'Super Admin') {
 ?>
 <li><a class="subheader">Management Dokter</a></li> 
 <li>  
  <a class="waves-effect" href="<?php echo base_url() . 'management_dokter' ?>"><i class="mdi mdi-menu"></i>Data Pasien
   <span class="badge badge-success pull-right"></span>
  </a>
 </li>
<?php } ?>

<?php if ($this->session->userdata('access') == 'IGD' || $this->session->userdata('access') == 'Super Admin') {
 ?>
 <li><a class="subheader">Management IGD / ICU</a></li> 
 <li>  
  <a class="waves-effect" href="<?php echo base_url() . 'management_igd' ?>"><i class="mdi mdi-menu"></i>Data Pasien
   <span class="badge badge-success pull-right"></span>
  </a>
 </li>
<?php } ?>

 <?php if ($this->session->userdata('access') == 'Farmasi' || $this->session->userdata('access') == 'Super Admin') {
 ?>
 <li><a class="subheader">Management Farmasi</a></li> 
 <li>  
  <a class="waves-effect" href="<?php echo base_url() . 'management_farmasi' ?>"><i class="mdi mdi-menu"></i>Data Pasien
   <span class="badge badge-success pull-right"></span>
  </a>
 </li>
<?php } ?>

<li><div class="divider"></div></li>
<li><a class="subheader">
  <img class="" src="<?php echo base_url() ?>files/img/dapps.png" width="20" height="20"/> &nbsp; Powered by &copy;DAppSolutions</a>
</li>
</ul>
var signaturePad;

var ManagementDokter = {
 module: function () {
  return 'management_dokter';
 },

 setDropifyInputFile: function () {
  $('.dropify').dropify();
 },

 uploadFoto: function (elm, e) {
  console.log($(elm));
 },

 addFile: function (elm) {
  var maksimal_foto = parseInt($('#maksimal_foto').val());
  var divControl = $(elm).closest('div.control');
//  var newDivControl = divControl.clone();
//  divControl.after(newDivControl);

  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(ManagementDokter.module()) + 'addContentFile',
   error: function () {
    message.error('.message', 'Error Program');
   },

   success: function (resp) {
//    newDivControl.html(resp);
    divControl.html(resp);
   }
  });
//  var divControl = $(elm).closest('div.controls');
//  var fileInput = divControl.find('input#input-file-now');
//  var newFileInput = divControl.clone();
//  divControl.after(newFileInput);
//  ManagementDokter.setDropifyInputFile();
 },

 clearTtd: function (e) {
  e.preventDefault();
  signaturePad.clear();
 },

 checkHasPoto: function () {
  var dokter = $('#id_dokter').val();
  
  var fotoPemeriksaan = $('.has-preview');
  var is_valid = false;  
  if (fotoPemeriksaan.length > 0) {
   is_valid = true;
  }else{
   if(dokter != ''){
    is_valid = true;
   }
  }

  return is_valid;
 },

 simpan: function () {
  if (validation.run()) {
   if (ManagementDokter.checkHasPoto()) {
    ManagementDokter.exec_save();
   }else{
    message.error('.message', 'Foto Hasil Pemeriksaan Tidak Ada');
   }
  }
 },

 getPostPasienHasPoto: function () {
  var fotoPemeriksaan = $('.has-preview');

  var data = [];
  $.each(fotoPemeriksaan, function () {
   var filename = $.trim($(this).find('span.dropify-filename-inner').text());
   var valueImage = $(this).find('span.dropify-render').find('img').attr('src');
   var keteranganFoto = $(this).closest('div.controls').find('#keterangan_foto').val();
   data.push({
    'filename': filename,
    'keterangan': keteranganFoto,
    'foto': valueImage
   });
  });

  return data;
 },

 getValueofTtd: function () {
  var valueTtd = '';
  if (signaturePad.isEmpty()) {
   message.error('.message', 'TTD Harus Diisi');
  } else {
   var dataURL = signaturePad.toDataURL("image/jpeg");
   valueTtd = dataURL;
  }

  return valueTtd;
 },

 get_post_data: function () {
  var dokter = $('#id_dokter').val();
  var data = {
   'id': $('#id').val(),
   'dokter': $('#id_dokter').val(),
   'receipt': $('#receipt').val(),
   'keterangan': $('#keterangan').val(),
   'pasien': $('#id').val(),
   'ttd': dokter == '' ? ManagementDokter.getValueofTtd() : '',
   'pasien_has_foto': dokter == '' ? ManagementDokter.getPostPasienHasPoto() : '',
  };

  return data;
 },

 setCanvasTtd: function () {
  var wrapper = document.getElementById("signature-pad");
  if(wrapper != null){
   var canvas = wrapper.querySelector("canvas");
  signaturePad = new SignaturePad(canvas, {
   // It's Necessary to use an opaque color when saving image as JPEG;
   // this option can be omitted if only saving as PNG or SVG
   backgroundColor: 'rgb(255, 255, 255)'
  });
  }  
 },

 exec_save: function () {
  var data = ManagementDokter.get_post_data();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));

  $.ajax({
   type: 'POST',
   data: formData,
   dataType: 'json',
   contentType: false,
   processData: false,
   async: false,
   url: url.base_url(ManagementDokter.module()) + 'save',
   error: function () {
    message.error('.message', 'Jaringan Error');
    message.closeLoading();
   },

   beforeSend: function(){
    message.loadingProses('Proses Simpan..');
   },
   
   success: function (resp) {
    if (resp.is_valid) {
     if ($('#id').val() != '') {
      message.success('.message', 'Data Berhasil Diperbaharui');
     } else {
      message.success('.message', 'Data Berhasil Disimpan');
     }
    } else {
     if ($('#id').val() != '') {
      message.error('.message', 'Data Gagal Diperbaharui');
     } else {
      message.error('.message', 'Data Gagal Disimpan');
     }
    }

    message.closeLoading();
    ManagementDokter.resetformInput();
    window.location.href = url.baseUrl(ManagementDokter.module())+'';
   }
  });
 },

 resetformInput: function () {
  var input = $('input');
  $.each(input, function () {
   $(this).val('');
  });
 },

 search: function (elm, e) {
  if (e.keyCode == 13 && $(elm).val() != '') {
   message.loadingProses('Proses Mendapatkan Data..');
   var keyword = $(elm).val();
   $.ajax({
    type: 'POST',
    data: {keyword: keyword},
    dataType: 'html',
    async: false,
    url: ManagementDokter.module() + '/search',
    success: function (resp) {
     $('.data').html(resp);
     helper.freezeHeaderTable();
     message.closeLoading();
    }
   });
  }
 },

 reloadPage: function () {
  window.location.reload();
 },
 
 showFoto: function(elm){
  console.log($(elm));
 }
};

$(function () {
 ManagementDokter.setDropifyInputFile();
 ManagementDokter.setCanvasTtd();
});
<div class="row-fluid"> 
 <div class="alert alert-success">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <?php if ($this->session->userdata('access_pengawas')) { ?>
   <h4>Dashboard</h4>
   Monitoring Data
  <?php } else { ?>
   <h4>Selamat Datang di Smart Rekam Medis</h4>   
  <?php } ?>
 </div> 
 <div class="row-fluid">
  <div class="card title-module">
   <div class="card-content">
    <i class="mdi mdi-arrow-left mdi-18px hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
    <i class="mdi mdi-arrow-right mdi-18px show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
    <a href="#" class="title-content"><?php echo $title ?></a>
    <hr/>
   </div>   
  </div>
 </div>

 <div class='row-fluid'>
  <div class='card'>
   <div class='card-content'>
    <div class="row">
     <div class='col-md-12'>
      <h4>Pasien</h4>
     </div>
     <div class="col-md-12">
      <input type='hidden' name='' id='data_pasien_hidup' class='form-control' value='<?php echo $data_pasien_hidup ?>'/>
      <input type='hidden' name='' id='data_pasien_meninggal' class='form-control' value='<?php echo $data_pasien_meninggal ?>'/>
      <canvas id="pasien"></canvas>
     </div>
     <div class='col-md-12'>
      <div class='text-right'>

       <a class="btn btn-warning" id="" 
          data-original-title="Print Data Pasien"
          href="<?php echo base_url() . $module . '/printPasien' ?>"
          onmouseover="message.showCustomTooltip(this, 'bottom')"                    
          >
        <i class="mdi mdi-printer"></i> &nbsp; Print
       </a>
      </div>
     </div>
    </div>
   </div>

  </div>
 </div>
</div>
<br/>
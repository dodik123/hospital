<div class="row-fluid">
 <div class="card title-module">
  <div class="card-content">
   <i class="mdi mdi-arrow-left mdi-18px hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
   <i class="mdi mdi-arrow-right mdi-18px show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
   <a href="#" class="title-content"><?php echo $title ?></a>
   <hr/>
  </div>
 </div>
</div>

<input type='hidden' name='' id='maksimal_foto' class='form-control' value='<?php echo $maksimal_foto ?>'/>
<input type='hidden' name='' id='id_dokter' class='form-control' value='<?php echo $userDokter ?>'/>
<div class="row-fluid">
 <!-- block -->
 <div class="card">
  <div class="card-content">
   <form class="form-horizontal">
    <fieldset>
     <legend><?php echo 'No. Registrasi : ' . $no_register ?></legend>
     <div class="message">

     </div>
     <input type="hidden" id="id" class="" value="<?php echo isset($id) ? $id : '' ?>"/>
     <div class="control-group">
      <label class="control-label" for="focusedInput">No. Registrasi</label>
      <div class="controls">
       <input class="input-xlarge focused required" id="no_register" type="text" 
              value="<?php echo isset($no_register) ? $no_register : '' ?>" 
              error = "No Register" placeholder="No. Register" disabled="">
      </div>
     </div>
     <div class="control-group">
      <label class="control-label" for="focusedInput">Nama Pasien</label>
      <div class="controls">
       <input class="input-xlarge focused required" id="nama" type="text" 
              value="<?php echo isset($nama) ? $nama : '' ?>" 
              error = "Nama" placeholder="Nama Siswa" disabled="">
      </div>
     </div>
     <div class="control-group">
      <label class="control-label">Alamat KTP</label>
      <div class="controls">
       <textarea id="alamat_ktp" disabled class="materialize-textarea"><?php echo isset($alamat_ktp) ? $alamat_ktp : '' ?></textarea>
      </div>
     </div>
     <div class="control-group">
      <label class="control-label">Alamat Sekarang</label>
      <div class="controls">
       <textarea id="alamat_sekarang" disabled class="materialize-textarea"><?php echo isset($alamat_sekarang) ? $alamat_sekarang : '' ?></textarea>
      </div>
     </div>
    </fieldset>
   </form>

   <form class="form-horizontal">
    <fieldset>
     <legend>Hasil Pemeriksaan Oleh Dokter : <?php echo $namaDokter ?></legend>
     <div class="control-group">
      <label class="control-label" for="focusedInput">Tanggal Pemeriksaan</label>
      <div class="controls">
       <input class="input-xlarge focused required" id="dokter" type="text" 
              value="<?php echo isset($tanggal_periksa) ? $tanggal_periksa : '' ?>" 
              error = "Tanggal Rekam Medis" placeholder="Tanggal Rekam Medis" disabled="">
      </div>
     </div>

     <div class="control-group">
      <label class="control-label" for="focusedInput">Ditangani Dokter</label>
      <div class="controls">
       <input class="input-xlarge focused required" id="dokter" type="text" 
              value="<?php echo isset($nama_dokter) ? $nama_dokter : '' ?>" 
              error = "Diperiksa Oleh Dokter" placeholder="Diperiksa Oleh Dokter" disabled="">
      </div>
     </div>

     <div class="control-group">
      <label class="control-label" for="focusedInput">Foto Hasil Pemeriksaan</label>      
      <?php if (isset($list_foto)) { ?>      
       <?php if (!empty($list_foto)) { ?>      
        <?php foreach ($list_foto as $value) { ?>
         <br/>
         <div class='controls'>
          <img id='foto' onclick="ManagementFarmasi.showFoto(this)" src="<?php echo $value['foto'] ?>" width="150" height="150"/>&nbsp;&nbsp;&nbsp;                          
          <label><?php echo $value['keterangan_foto'] ?></label>
         </div>      
        <?php } ?>
       <?php } ?>
      <?php } else { ?>
       <br/>      
       <label class="control-label" for="focusedInput">Maksimal Foto (<?php echo $maksimal_foto ?>)</label>            
       <?php for ($i = 0; $i < $maksimal_foto; $i++) { ?>
        <div class="controls">
         <input type="file" id="input-file-now" class="dropify" data-height="60" 
                onchange="ManagementFarmasi.uploadFoto(this, event)"/>
         &nbsp;
         <br/>
         <input type='text' id='keterangan_foto' class='input-xlarge focused' value='' disabled=""/>
         <label for="keterangan_foto">(Keterangan Foto: City Scan, Dll)</label>
        </div>
       <?php } ?>
      <?php } ?>
     </div>

     <div class="control-group">
      <label class="control-label" for="focusedInput">Keterangan</label>
      <div class="controls">
       <textarea disabled id="keterangan" class="materialize-textarea"><?php echo isset($keterangan) ? $keterangan : '' ?></textarea>
       <label for="keterangan">(Saran, Dll)</label>
      </div>
     </div>

     <div class="control-group">
      <label class="control-label" for="focusedInput">Resep Obat</label>
      <div class="controls">
       <textarea id="receipt" disabled error="Resep" class="materialize-textarea required"><?php echo isset($receipt) ? $receipt : '' ?></textarea>
       <label for="receipt">(Resep Obat / Jika Tidak Ada Beri Keterangan : 'Tidak Ada Resep')</label>
      </div>
     </div>

     <div class="control-group">
      <label class="control-label" for="focusedInput">Tanda Tangan Dokter</label>
      <div class="controls">
       <?php if ($id_doctor == '') { ?>
        <div id="signature-pad" class="signature-pad">
         <div class="signature-pad--body">
          <canvas id='canvas_ttd' style="border:1px solid #999;"></canvas>
         </div>           
         <label for="canvas_ttd">(Tanda Tangan)</label>
         <button id="" onclick="ManagementFarmasi.clearTtd(event)" class="btn btn-warning" >Clear</button>
        </div>     
       <?php } else { ?>
        <img onclick="ManagementFarmasi.showFoto(this)" src="<?php echo $ttd ?>" width="150" height="150"/>
       <?php } ?>
       &nbsp;       
      </div>      
     </div>

     <div class='row'>
      <div class='col-md-12'>
       <h5>Keputusan Pengananan Pasien</h5>
       <hr/>
      </div>
     </div>
     <div class="control-group">
      <label class="control-label" for="focusedInput">Keputusan Penanganan Pasien (IDG / Poli)</label>      
      <div class="controls">
       <?php if ($status_pasien == '') { ?>
        <select error="Keputusan Penanganan Pasien" id="status_pasien" class="input-xlarge focused required">
         <option value="">--Keputusan Penanganan Pasien--</option>
         <?php if (!empty($list_status)) { ?>
          <?php foreach ($list_status as $value) { ?>
           <option value="<?php echo $value['id'] ?>"><?php echo $value['status'] ?></option>
          <?php } ?>
         <?php } ?>
        </select>
       <?php } else { ?>
        <input type='text' name='' id='' disabled="" class='input-xlarge focused' value='<?php echo $st_pasien ?>'/>
       <?php } ?>
      </div>
     </div>

     <div class="text-right">
      <?php if ($status_pasien != '') { ?>
       <?php if ($this->session->userdata('access') == 'Farmasi') { ?>
        <button type="button" class="btn btn-primary" onclick="ManagementFarmasi.simpan()">Approve</button>
       <?php } ?>
      <?php } ?>
      <a href="<?php echo base_url() . $module ?>"><button type="button" class="btn btn-success">Kembali</button></a>
     </div>
    </fieldset>
   </form>
  </div>
 </div>
</div>
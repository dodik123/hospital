<?php

class Management_igd extends MX_Controller {

 public function __construct() {
  parent::__construct();
  if ($this->session->userdata('id') == '') {
   redirect(base_url());
  }
 }

 public function getModuleName() {
  return 'management_igd';
 }

 public function getTableName() {
  return 'pasien';
 }

 public function getHeaderJSandCSS() {
  $data = array(
  '<script src="' . base_url() . 'assets/js/signature_pad.umd.js"></script>',
//   '<script src="' . base_url() . 'assets/js/app.js"></script>',
  '<script src="' . base_url() . 'assets/js/message.js"></script>',
  '<script src="' . base_url() . 'assets/js/validation.js"></script>',
  '<script src="' . base_url() . 'assets/js/url.js"></script>',
  '<script src="' . base_url() . 'assets/js/controllers/' . $this->getModuleName() . '.js"></script>'
  );

  return $data;
 }

 public function index() {
  $base_url = base_url() . $this->getModuleName() . '/index/';
  $total_rows = count($this->getDataManagement_igd());
  $offset = $this->uri->segment(3);
  $data['view_file'] = 'management_igd_index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['jumlah_data_management_igd'] = count($this->getDataManagement_igd());
  $data['title'] = ucwords(str_replace('_', ' ', "Data Pasien"));
  $data['data_management_igd'] = $this->getDataManagement_igd(5, $offset);
  $data['pagination'] = Modules::run('pagination/set_paging', $base_url, base_url() . $this->getModuleName(), $total_rows, 5);

  echo Modules::run('template', $data);
 }

 public function getNamaDokter() {
  $id = $this->session->userdata('id');
  $data = Modules::run('database/get', array(
  'table' => 'dokter',
  'where' => array('user' => $id)
  ));

  $nama = 'Bukan Dokter';
  if (!empty($data)) {
   $data = $data->row_array();
   $nama = $data['nama'];
  }
  return $nama;
 }

 public function getJumlahFotoMaksimal() {
  $data = Modules::run('database/get', array(
  'table' => 'maksimal_foto'
  ));

  $jumlah = 2;
  if (!empty($data)) {
   $data = $data->row_array();
   $jumlah = $data['jumlah'];
  }

  return $jumlah;
 }

 public function periksaPasien($id) {
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' p',
  'field' => array('p.*', 'phd.receipt',
  'phd.keterangan', 'doct.id as id_doctor',
  'usr_dokter.id as userDokter',
  'doct.nama as namaDokter', 'phd.createddate as tanggal_periksa'),
  'join' => array(
  array('pasien_has_dokter phd', 'p.id = phd.pasien', 'left'),
  array('user usr_dokter', 'phd.user = usr_dokter.id', 'left'),
  array('dokter doct', 'usr_dokter.id = doct.user', 'left'),
  ),
  'where' => array('p.id' => $id)
  ))->row_array();
  $data['tanggal_periksa'] = $data['id_doctor'] == '' ? date('d M Y') :
  date('d M Y', strtotime($data['tanggal_periksa']));
  $data['has_periksa'] = $data['id_doctor'] == '' ? false : true;
  $data['nama_dokter'] = $data['id_doctor'] == '' ? $this->getNamaDokter() : $data['namaDokter'];
  $data['maksimal_foto'] = $this->getJumlahFotoMaksimal();
//  echo '<pre>';
//  print_r($data);
//  die;
  $data['view_file'] = 'management_igd_add_view';
  $data['module'] = $this->getModuleName();
  $data['title'] = ucwords(str_replace('_', ' ', 'Form Pemeriksaan Pasien'));
  $data['header_data'] = $this->getHeaderJSandCSS();
  echo Modules::run('template', $data);
 }

 public function getPasienFotoPemeriksaan($pasien) {
  $data = Modules::run('database/get', array(
  'table' => 'pasien_has_foto',
  'where' => array('pasien' => $pasien)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getListStatusPasien() {
  $data = Modules::run('database/get', array(
  'table' => 'status_pasien'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function editPasien($id) {
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' p',
  'field' => array('p.*', 'phd.receipt',
  'phd.keterangan', 'doct.id as id_doctor',
  'doct.nama as namaDokter',
  'usr_dokter.id as userDokter',
  'phd.ttd',
  'st.status as st_pasien',
  'phd.createddate as tanggal_periksa'),
  'join' => array(
  array('pasien_has_dokter phd', 'p.id = phd.pasien'),
  array('user usr_dokter', 'phd.user = usr_dokter.id'),
  array('dokter doct', 'usr_dokter.id = doct.user'),
  array('status_pasien st', 'p.status_pasien = st.id', 'left'),
  ),
  'where' => array('p.id' => $id)
  ))->row_array();
  $data['tanggal_periksa'] = $data['id_doctor'] == '' ? date('d M Y') :
  date('d M Y', strtotime($data['tanggal_periksa']));
  $data['has_periksa'] = $data['id_doctor'] == '' ? false : true;
  $data['nama_dokter'] = $data['id_doctor'] == '' ? $this->getNamaDokter() : $data['namaDokter'];
  $data['maksimal_foto'] = $this->getJumlahFotoMaksimal();
  $data['list_foto'] = $this->getPasienFotoPemeriksaan($data['id']);
  $data['list_status'] = $this->getListStatusPasien();
//  echo '<pre>';
//  print_r($data['list_foto']);
//  die;
  $data['view_file'] = 'management_igd_add_view';
  $data['module'] = $this->getModuleName();
  $data['title'] = ucwords(str_replace('_', ' ', 'Form Hasil Pemeriksaan Pasien'));
  $data['header_data'] = $this->getHeaderJSandCSS();
  echo Modules::run('template', $data);
 }

 public function search() {
  $data['module'] = $this->getModuleName();
  $data['data_management_igd'] = $this->getDataManagement_igd();
  echo $this->load->view('management_igd_search_view', $data, true);
 }

 public function getDataManagement_igd($limit = '', $offset = '') {
  $keyword = $this->input->post('keyword');
  $query['table'] = $this->getTableName() . ' p';
  $query['field'] = array('p.*', 'phd.id as dokter_id', 'doct.nama as nama_dokter', 
  'st.status as st_pasien');
  $query['join'] = array(
  array('pasien_has_dokter phd', 'p.id = phd.pasien'),
  array('user user_dok', 'phd.user = user_dok.id'),
  array('dokter doct', 'user_dok.id = doct.user'),
  array('status_pasien st', 'p.status_pasien = st.id', 'left'),
  );
  if ($limit != '') {
   $query['limit'] = $limit;
   $query['offset'] = $offset;
  }
  $query['orderby'] = "p.id desc";

  if ($keyword != '') {
   $query['is_or_like'] = true;
   $query['inside_brackets'] = true;
   $query['like'] = array(
   array('p.id', $keyword),
   array('p.no_register', $keyword),
   array('p.nama', $keyword),
   array('doct.nama', $keyword),
   array('st.status', $keyword),
   );
  }

  $data = Modules::run('database/get', $query);
//  echo '<pre>';
//  echo $this->db->last_query();
//  die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['tanggal_registrasi'] = date('d M Y', strtotime($value['tanggal_registrasi']));
    $value['status'] = $value['dokter_id'] == '' ? 'Belum Ada Pemeriksaan' : 'Diperiksa Oleh : ' . $value['nama_dokter'];
    $value['st_pasien'] = $value['status_pasien'] == '' ? 'Belum Ada Keputusan' : $value['st_pasien'];
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function isExistDataApproval($pasien) {
  $data = Modules::run('database/get', array(
  'table' => 'pasien_approved_user',
  'where' => array('pasien' => $pasien,
  'user' => $this->session->userdata('id'))
  ));

  $is_exist = false;
  if (!empty($data)) {
   $is_exist = true;
  }

  return $is_exist;
 }

 public function save() {
  $data = json_decode($this->input->post('data'));
  $is_valid = false;

  $this->db->trans_begin();
  try {
   Modules::run('database/_update', 'pasien', array('status_pasien' => $data->status_pasien), array('id' => $data->pasien));
   $is_exist_approve_user = $this->isExistDataApproval($data->pasien);
   if (!$is_exist_approve_user) {
    $data_pasien_approved['pasien'] = $data->pasien;
    $data_pasien_approved['user'] = $this->session->userdata('id');
    Modules::run('database/_insert', 'pasien_approved_user', $data_pasien_approved);
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}

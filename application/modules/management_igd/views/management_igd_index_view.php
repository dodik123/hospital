<div class="row-fluid">
 <div class="card title-module">
  <div class="card-content">
   <i class="mdi mdi-arrow-left mdi-18px hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
   <i class="mdi mdi-arrow-right mdi-18px show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
   <a href="<?php echo base_url() . $module ?>" class="title-content"><?php echo $title ?></a>
   <hr/>
  </div>
 </div>
</div>

<div class="row-fluid">
 <div class="card">
  <div class="card-content">
   <div class="">
    <div class="table-toolbar">
     <!--     <div class="btn-group">
           <a style="margin-left: 12px;" href="<?php echo base_url() . $module . '/add' ?>"><button class="btn btn-primary">Tambah <i class="icon-plus icon-white"></i></button></a>
          </div>-->
    </div>
    <br/>

    <div class="table-toolbar">
     <div class="btn-group pull-right">
      <input class="input-xlarge focused" id="search" type="text" value="" 
             placeholder="Pencarian" onkeyup="ManagementIgd.search(this, event)">
     </div>
    </div>

    <br/>
    <br/>

    <div class="message">

    </div>
    <div class="data">
     <div class="sticky-table sticky-headers sticky-ltr-cells">
      <table cellpadding="0" cellspacing="0" border="0" class="" id="tabel_nilai">
       <thead>
        <tr class="sticky-row">
         <th>No</th>
         <th>No Register</th>
         <th>Nama Pasien</th>
         <th>Tanggal Registrasi</th>
         <th>Status</th>
         <th>Keputusan Pengananan Pasien</th>
         <th>Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($data_management_igd)) { ?>
         <?php $no = $this->uri->segment(3) + 1; ?>
         <?php foreach ($data_management_igd as $value) { ?>
          <tr class="odd gradeX">
           <td><?php echo $no++ ?></td>
           <td><?php echo $value['nama'] ?></td>
           <td><?php echo $value['no_register'] ?></td>
           <td><?php echo $value['tanggal_registrasi'] ?></td>
           <td><?php echo $value['status'] ?></td>
           <td><?php echo $value['st_pasien'] ?></td>
           <td>
            <?php if ($value['dokter_id'] == '') { ?>
             <a class="btn btn-warning" id="" 
                data-original-title="Periksa Pasien"
                href="<?php echo base_url() . $module . '/periksaPasien/' . $value['id'] ?>"
                onmouseover="message.showCustomTooltip(this, 'left')">
              <i class="mdi mdi-medical-bag"></i>
             </a>&nbsp;&nbsp;&nbsp;
            <?php } ?>
            <a class="btn btn-success" id="" 
                    data-original-title="Edit Data Pasien"
                    href="<?php echo base_url() . $module . '/editPasien/' . $value['id'] ?>"
                    onmouseover="message.showCustomTooltip(this, 'left')"                    
                    >
             <i class="mdi mdi-pencil-box"></i>
            </a>
           </td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td colspan="12">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>
       </tbody>
      </table>
     </div>  

     <div class="paging text-right">
      <?php echo $pagination ?>
     </div>
    </div>        
   </div>
  </div>
 </div>
</div>
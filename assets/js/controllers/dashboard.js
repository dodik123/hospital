var Dashboard = {
 module: function () {
  return 'dashboard';
 },

 onCloseWindow: function () {
  alert('asdadasd');
 },

 hover: function (elm) {
  $(elm).addClass('tr_hover');
 },

 mouseOut: function (elm) {
  $(elm).removeClass('tr_hover');
 },

 getDataPasienHidup: function () {
  var data_pasien_hidup = $('#data_pasien_hidup').val();
  var pasien = data_pasien_hidup.toString().split(',');

  var data_all = [];
  for (var i = 0; i < 12; i++) {
   var value = parseFloat((pasien[i])).toFixed(2);
   data_all.push(value);
  }

  return data_all;
 },

 getDataPasienMeninggal: function () {
  var data_pasien_meninggal = $('#data_pasien_meninggal').val();
  var pasien = data_pasien_meninggal.toString().split(',');

  var data_all = [];
  for (var i = 0; i < 12; i++) {
   var value = parseFloat((pasien[i])).toFixed(2);
   data_all.push(value);
  }

  return data_all;
 },

 setGrafikPasien: function () {
  var ctx = document.getElementById('pasien').getContext('2d');
  var barChartData = {
   labels: ['Januari', 'Februari', 'Maset', 'April', 'Mei', 'Juni',
    'Juli', 'Agustus', 'Sepetember', 'Oktober', 'November', 'Desember'],
   datasets: [{
     label: 'Pasien Meninggal',
     backgroundColor: window.chartColors.red,
     yAxisID: 'pasien_meninggal',
     data: Dashboard.getDataPasienMeninggal()
    }, {
     label: 'Pasien Hidup',
     backgroundColor: window.chartColors.blue,
     yAxisID: 'pasien_hidup',
     data: Dashboard.getDataPasienHidup()
    }]
  };

  window.myBar = new Chart(ctx, {
   type: 'bar',
   data: barChartData,
   options: {
    responsive: true,
    title: {
     display: true,
     text: 'Grafik Pasien'
    },
    tooltips: {
     mode: 'index',
//    intersect: true
    },
    scales: {
     yAxes: [{
       type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
       display: true,
       position: 'left',
       id: 'pasien_meninggal'
      }, {
       type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
       display: true,
       position: 'left',
       id: 'pasien_hidup'
      }]
    }
   }
  });
 },
};

$(function () {
 Dashboard.setGrafikPasien();
// window.addEventListener("beforeunload", function (event) {
//  event.preventDefault();
// });
});
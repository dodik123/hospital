<div class="sticky-table sticky-headers sticky-ltr-cells">
 <table cellpadding="0" cellspacing="0" border="0" class="" id="tabel_nilai">
  <thead>
   <tr class="sticky-row">
    <th>No</th>
    <th>No Register</th>
    <th>Nama Pasien</th>
    <th>Tanggal Registrasi</th>
    <th>Status</th>
    <th>Keputusan Pengananan Pasien</th>
    <th>Status Approval</th>
    <th>Action</th>
   </tr>
  </thead>
  <tbody>
   <?php if (!empty($data_management_farmasi)) { ?>
    <?php $no = $this->uri->segment(3) + 1; ?>
    <?php foreach ($data_management_farmasi as $value) { ?>
     <tr class="odd gradeX">
      <td><?php echo $no++ ?></td>
      <td><?php echo $value['nama'] ?></td>
      <td><?php echo $value['no_register'] ?></td>
      <td><?php echo $value['tanggal_registrasi'] ?></td>
      <td><?php echo $value['status'] ?></td>
      <td><?php echo $value['st_pasien'] ?></td>
      <td><?php echo $value['st_approval'] ?></td>
      <td>
       <?php if ($value['dokter_id'] == '') { ?>
        <a class="btn btn-warning" id="" 
           data-original-title="Periksa Pasien"
           href="<?php echo base_url() . $module . '/periksaPasien/' . $value['id'] ?>"
           onmouseover="message.showCustomTooltip(this, 'left')">
         <i class="mdi mdi-medical-bag"></i>
        </a>&nbsp;&nbsp;&nbsp;
       <?php } ?>
       <a class="btn btn-success" id="" 
          data-original-title="Edit Data Pasien"
          href="<?php echo base_url() . $module . '/editPasien/' . $value['id'] ?>"
          onmouseover="message.showCustomTooltip(this, 'left')"                    
          >
        <i class="mdi mdi-pencil-box"></i>
       </a>
      </td>
     </tr>
    <?php } ?>
   <?php } else { ?>
    <tr>
     <td colspan="12">Tidak Ada Data Ditemukan</td>
    </tr>
   <?php } ?>
  </tbody>
 </table>
</div>  
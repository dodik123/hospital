<?php

class Pasien extends MX_Controller {

 public function __construct() {
  parent::__construct();
  if ($this->session->userdata('id') == '') {
   redirect(base_url());
  }
 }

 public function getModuleName() {
  return 'pasien';
 }

 public function getTableName() {
  return 'pasien';
 }

 public function getHeaderJSandCSS() {
  $data = array(
   '<script src="' . base_url() . 'assets/js/message.js"></script>',
   '<script src="' . base_url() . 'assets/js/validation.js"></script>',
   '<script src="' . base_url() . 'assets/js/url.js"></script>',
   '<script src="' . base_url() . 'assets/js/controllers/' . $this->getModuleName() . '.js"></script>'
  );

  return $data;
 }

 public function index() {
  $base_url = base_url() . $this->getModuleName() . '/index/';
  $total_rows = count($this->getDataPasien());
  $offset = $this->uri->segment(3);
  $data['view_file'] = 'pasien_index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['jumlah_data_pasien'] = count($this->getDataPasien());
  $data['title'] = ucwords(str_replace('_', ' ', $this->getModuleName()));
  $data['data_pasien'] = $this->getDataPasien("", 5, $offset);  
  $data['pagination'] = Modules::run('pagination/set_paging', 
  $base_url, base_url() . $this->getModuleName(), 
   $total_rows, 5);
  
  echo Modules::run('template', $data);
 }

 public function search() {
  $data['module'] = $this->getModuleName();
  $data['data_pasien'] = $this->getDataPasien();
  echo $this->load->view('pasien_search_view', $data, true);
 }

 public function add() {
  $data['view_file'] = 'pasien_add_view';
  $data['module'] = $this->getModuleName();
  $data['title'] = ucwords(str_replace('_', ' ', $this->getModuleName()));
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['list_jurusan'] = array();
  echo Modules::run('template', $data);
 }
 
 public function detailAllPasien($ujian, $limit = '', $offset = '') {
  $data['ujian'] = $ujian;
  $data['module'] = $this->getModuleName();
  $data['data_pasien'] = $this->getDataPasien($ujian);
  echo $this->load->view('pasien_detailAllPasien_view', $data, true);
 }

 public function getDataPasien($ujian = "", $limit = '', $offset = '') {
  $keyword = $this->input->post('keyword');
  $siswa = $this->session->userdata('id');
  $access = $this->session->userdata('access');

  $query['table'] = $this->getTableName();
  
  if ($limit != '') {
   $query['limit'] = $limit;
   $query['offset'] = $offset;
  }
  $query['orderby'] = "id desc";

  if ($keyword != '') {
   $query['is_or_like'] = true;
   $query['inside_brackets'] = true;
   $query['like'] = array(
    array('id', $keyword),
    array('no_register', $keyword),
    array('nama', $keyword),
   );
  }

  $data = Modules::run('database/get', $query);
//  echo '<pre>';
//  echo $this->db->last_query();
//  die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getJawabanSiswa($ujian, $siswa) {
//  $siswa = $this->session->userdata('id');
  $data = Modules::run('database/get', array(
    'table' => 'siswa_has_jawaban shj',
    'field' => array('shj.id', 'so_hj.true_or_false as status_jawaban',
     'uhs.soal', 'so_hj.jawaban', 'uhs.id as soal_id',
     'so_hj.file_jawaban', 's.file_soal'),
    'join' => array(
     array('soal_has_jawaban so_hj', 'shj.soal_has_jawaban = so_hj.id'),
     array('soal s', 'so_hj.soal = s.id'),
     array('ujian_has_soal uhs', 's.id = uhs.soal')
    ),
    'where' => array('shj.siswa' => $siswa, 'uhs.ujian' => $ujian)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['jawaban_benar'] = $this->getTrueAnswerSoal($value['soal']);
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getTrueAnswerSoal($soal) {
  $data = Modules::run('database/get', array(
    'table' => 'soal_has_jawaban',
    'where' => array('soal' => $soal, 'true_or_false' => true)
  ));

  $answer = "";
  if (!empty($data)) {
   $data = $data->row_array();
   $answer = $data['jawaban'];
  }

  return $answer;
 }

 public function detailJawaban($ujian, $siswa) {
  $data['data_jawaban'] = $this->getJawabanSiswa($ujian, $siswa);
  echo $this->load->view('pasien_detailJawaban_view', $data, true);
 }

 public function downloadPDFPasien($ujian) {
  $this->load->library('m_pdf');
  $pdf = $this->m_pdf->load();
  $pdf = new mPDF('A4');
  $data['ujian'] = Modules::run('ujian_selesai_dilaksanakan/getDataUjian', $ujian);
  $data['data_pasien'] = $this->getDataPasien($ujian);
  $view = $this->load->view('pasien_downloadPDFPasien_view', $data, true);
  $pdf->WriteHTML($view);
  $pdf->Output('Pasien - ' . $data['ujian']['kode_ujian'] . '.pdf', 'I');
 }

 public function downloadExcel($ujian) {
  $this->load->library('PHPExcel', array());
  $this->load->library('PHPExcel/IOFactory', array());
  $excel = new PHPExcel();
  //set border     
  $bStyle = array(
   'borders' => array(
    'allborders' => array(
     'style' => PHPExcel_Style_Border::BORDER_THIN
    )
   )
  );
  $bStyle_out = array(
   'borders' => array(
    'top' => array(
     'style' => PHPExcel_Style_Border::BORDER_THIN
    ),
    'left' => array(
     'style' => PHPExcel_Style_Border::BORDER_THIN
    ),
    'right' => array(
     'style' => PHPExcel_Style_Border::BORDER_THIN
    ),
   ),
   'fill' => array(
    'type' => PHPExcel_Style_Fill::FILL_SOLID,
//  'color' => array('rgb' => 'E1E0F7'),
   )
  );
  $bStyle_left_right = array(
   'borders' => array(
    'left' => array(
     'style' => PHPExcel_Style_Border::BORDER_THIN
    ),
    'right' => array(
     'style' => PHPExcel_Style_Border::BORDER_THIN
    )
   ),
   'fill' => array(
    'type' => PHPExcel_Style_Fill::FILL_SOLID,
//  'color' => array('rgb' => 'E1E0F7'),
   )
  );

  $excel->getActiveSheet()->getStyle()->applyFromArray($bStyle_out);
  $excel->getActiveSheet()->setCellValue('A1', 'No');
  $excel->getActiveSheet()->setCellValue('B1', 'Nama');
  $excel->getActiveSheet()->setCellValue('C1', 'Nis');
  $excel->getActiveSheet()->setCellValue('D1', 'Jurusan');
  $excel->getActiveSheet()->setCellValue('E1', 'Pasien');

  $list_pasien = $this->getDataPasien($ujian);
  $kode_ujian = '';
  if (count($list_pasien) > 0) {
   $no = 1;
   $counter = 2;
   foreach ($list_pasien as $value) {
    $kode_ujian = $value['kode_ujian'];
    $excel->getActiveSheet()->setCellValue('A' . $counter, $no++);
    $excel->getActiveSheet()->setCellValue('B' . $counter, $value['siswa']);
    $excel->getActiveSheet()->setCellValue('C' . $counter, $value['nis']);
    $excel->getActiveSheet()->setCellValue('D' . $counter, $value['jurusan']);
    $excel->getActiveSheet()->setCellValue('E' . $counter, number_format($value['pasien'], 2));
    $counter += 1;
   }
  }
  header('Content-Disposition: attachment;filename="DaftarPasienUjian_' . $kode_ujian . '.xlsx"');
//  $objWrite = IOFactory::createWriter($excel, 'Excel2007');
  $objWrite = IOFactory::createWriter($excel, 'Excel2007');
  $objWrite->save('php://output');
 }

}

<?php

class Login extends MX_Controller {

 public function index() {
//  echo $this->load->view('login_index_view', array(), true);
  echo $this->load->view('login_new', array(), true);
 }

 public function getDataUser($username, $password) {
  $data = Modules::run('database/get', array(
  'table' => 'user usr',
  'field' => array('usr.*', 'pv.hak_akses'),
  'join' => array(
  array('priveledge pv', 'usr.priveledge = pv.id')
  ),
  'where' => array('usr.username' => $username,
  'usr.password' => $password)
  ));

  return $data;
 }

 public function sign_in() {
  $username = $this->input->post('username');
  $password = $this->input->post('password');

  $is_valid = false;
  $is_siswa = false;
  try {
   $data = $this->getDataUser($username, $password);
   if (!empty($data)) {
    $data = $data->row_array();
    $is_valid = true;
   }
  } catch (Exception $exc) {
   $is_valid = false;
  }

  if ($is_valid) {
   $this->setSessionData($data);
  }
  echo json_encode(array('is_valid' => $is_valid));
 }

 public function setSessionData($data) {
  $session['id'] = $data['id'];
  $session['username'] = $data['username'];
  $session['access'] = $data['hak_akses'];
  $this->session->set_userdata($session);
 }

 public function sign_out() {
  $ujian = $this->session->userdata('ujian');
  $id = $this->session->userdata('id');
  if ($ujian != '') {
   $username = $this->getNisSiswa($id);
   $this->updateStatusSiswaLogin($username);
  }

  $this->session->sess_destroy();
  redirect(base_url());
 }

}

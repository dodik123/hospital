var ManagementFarmasi = {
 module: function () {
  return 'management_farmasi';
 },

 simpan: function () {
  if (validation.run()) {
   ManagementFarmasi.exec_save();
  }
 },

 get_post_data: function () {
  var dokter = $('#id_dokter').val();
  var data = {
   'id': $('#id').val(),
   'pasien': $('#id').val()
  };

  return data;
 },

 exec_save: function () {
  var data = ManagementFarmasi.get_post_data();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));

  $.ajax({
   type: 'POST',
   data: formData,
   dataType: 'json',
   contentType: false,
   processData: false,
   async: false,
   url: url.base_url(ManagementFarmasi.module()) + 'save',
   error: function () {
    message.error('.message', 'Jaringan Error');
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses('Proses Simpan..');
   },

   success: function (resp) {
    if (resp.is_valid) {
     if ($('#id').val() != '') {
      message.success('.message', 'Data Berhasil Diperbaharui');
     } else {
      message.success('.message', 'Data Berhasil Disimpan');
     }
    } else {
     if ($('#id').val() != '') {
      message.error('.message', 'Data Gagal Diperbaharui');
     } else {
      message.error('.message', 'Data Gagal Disimpan');
     }
    }

    message.closeLoading();
    ManagementFarmasi.resetformInput();
    window.location.href = url.baseUrl(ManagementFarmasi.module()) + '';
   }
  });
 },

 resetformInput: function () {
  var input = $('input');
  $.each(input, function () {
   $(this).val('');
  });
 },

 search: function (elm, e) {
  if (e.keyCode == 13 && $(elm).val() != '') {
   message.loadingProses('Proses Mendapatkan Data..');
   var keyword = $(elm).val();
   $.ajax({
    type: 'POST',
    data: {keyword: keyword},
    dataType: 'html',
    async: false,
    url: ManagementFarmasi.module() + '/search',
    success: function (resp) {
     $('.data').html(resp);
     helper.freezeHeaderTable();
     message.closeLoading();
    }
   });
  }
 },

 reloadPage: function () {
  window.location.reload();
 },

 showFoto: function (elm) {
  console.log($(elm));
 }
};

$(function () {
});